package controllers;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class BaseController {
    private String layout = "_layout";

    protected ModelAndView view(ModelAndView modelAndView) {
        modelAndView.addObject("body", modelAndView.getViewName());
        modelAndView.setViewName(layout);
        return modelAndView;
    }

    protected String view(String viewName, Model model) {
        model.addAttribute("body", viewName);
        return layout;
    }
}
