package controllers;

import models.Dept;
import models.Employe;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import services.DeptService;
import services.EmployeService;
import services.api.IDeptService;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

@Controller
public class IndexController extends BaseController {
    private EmployeService empservice;
    private IDeptService   deptService;

    public IndexController(EmployeService emp, DeptService dept) {
        this.empservice = emp;
        this.deptService = dept;
    }

    @RequestMapping(value = "//")
    public ModelAndView index(ModelAndView modelAndView) {
        modelAndView.setViewName("index");
        return view(modelAndView);
    }

    @RequestMapping(value = "/configuration-data-base")
    public ModelAndView configuration(ModelAndView modelAndView) {
        modelAndView.setViewName("configuration");
        return view(modelAndView);
    }

    @RequestMapping(value = {"/create-dept", "update-dept/{id}"})
    public ModelAndView createDept(ModelAndView modelAndView, @PathVariable(required = false) Integer id) {
        try {
            modelAndView.setViewName("create-dept");
            modelAndView.addObject("deptsName", deptService.deptsName());
            modelAndView.addObject("dept", deptService.getDeptByNo(id == null ? 0 : id));
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return view(modelAndView);
    }

    @RequestMapping(value = "/create-employe")
    public ModelAndView createEmploye(ModelAndView modelAndView) {
        modelAndView.setViewName("create-employe");
        return view(modelAndView);
    }

    @RequestMapping(value = "/list-dept")
    public ModelAndView listDept(ModelAndView modelAndView) {
        try {
            List<Dept> depts = deptService.findListDepts();
            modelAndView.addObject("depts", depts);
        } catch (Exception e) {
            e.printStackTrace();
        }
        modelAndView.setViewName("list-dept");
        return view(modelAndView);
    }

    @RequestMapping(value = "/info-dept")
    public ModelAndView infoDetp(@RequestParam int id, ModelAndView modelAndView) {
        try {
            Dept dept = deptService.getDeptByNo(id);
            modelAndView.addObject("dept", dept);
            modelAndView.addObject("employes", dept.getEmployes());
            modelAndView.setViewName("info-dept");
        } catch (Exception e) {
            e.printStackTrace();
        }
        return view(modelAndView);
    }

    @RequestMapping(value = "/list-employe")
    public ModelAndView listEmploye(ModelAndView modelAndView) {
        modelAndView.setViewName("list-employe");
        ArrayList<Employe> employes = null;
        try {
            employes = empservice.getAllEmployes();
        } catch (SQLException | ClassNotFoundException e) {
            e.printStackTrace();
        }
        modelAndView.addObject("employes", employes);
        return view(modelAndView);
    }

    @RequestMapping(value = "/insert-or-update-dept/{update}")
    public ModelAndView insertOrUpdate(@ModelAttribute("dept") Dept dept, @PathVariable boolean update, ModelAndView modelAndView) {
        try {
            deptService.saveOrUpdate(dept, update);
            modelAndView.setViewName("redirect:/list-dept");
        } catch (Exception e) {
            e.printStackTrace();
        }
        return modelAndView;
    }

    @RequestMapping(value = "/show-employe")
    public ModelAndView showEmploye(ModelAndView modelAndView) {
        modelAndView.setViewName("show-employe");
        return view(modelAndView);
    }

    @RequestMapping(value = "/add-new-employe")
    public ModelAndView addNewEmploye(ModelAndView modelAndView) {
        try {
            modelAndView.setViewName("add-new-employe");
            empservice.save(null);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return view(modelAndView);
    }

    @RequestMapping(value = "/delete-dept/{id}")
    public ModelAndView deleteDept(ModelAndView modelAndView, @PathVariable(required = false) Integer id) {
        try {
            deptService.deleteByNo(id);
            modelAndView.setViewName("redirect:/list-dept");
        } catch (Exception e) {
            e.printStackTrace();
        }
        return modelAndView;
    }
}
