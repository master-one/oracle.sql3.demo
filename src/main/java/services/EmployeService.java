package services;

import models.Employe;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Map;

public class EmployeService extends BaseService {

    public EmployeService() {
    }


    public Employe save(Employe employe) {
        return null;
    }

    public ArrayList<Employe> getAllEmployes() throws SQLException, ClassNotFoundException {
        ArrayList<Employe> reponse          = new ArrayList<>();
        Employe temp=new Employe();
        ResultSet          resultsetTableau = null;
        try (Connection conn = getConnexion().openConnection();
             Statement stmt = conn.createStatement()) {
            Map<String, Class<?>> mapOraObjType = conn.getTypeMap();
            mapOraObjType.put("EMPLOYE_T", Class.forName("models.Employe"));
            String sqlTableau = "SELECT value(ot) FROM employe_o ot";
            resultsetTableau = stmt.executeQuery(sqlTableau);
            while (resultsetTableau.next()) {
                temp = (Employe) resultsetTableau.getObject(1, mapOraObjType);
                reponse.add(temp);
                /*String prenom ="";
                String [] lesprenoms= (String [])temp.getPrenoms().getArray();
                for (int i=0; i<lesprenoms.length;i++){
                    prenom =  prenom+" "+lesprenoms[i];
                }
                temp.lesprenoms= prenom;*/
            }
        } finally {
            if (resultsetTableau != null)
                resultsetTableau.close();
        }
        return reponse;
    }
}
