package services;

import configuration.Connexion;

public class BaseService {
    private Connexion connexion;

    public Connexion getConnexion() {
        return connexion;
    }

    public void setConnexion(Connexion connexion) {
        this.connexion = connexion;
    }
}
