package services.api;

import models.Dept;

import java.sql.SQLException;
import java.util.List;

public interface IDeptService {
    List<Dept> findListDepts() throws SQLException;

    Dept getDeptByNo(int id) throws SQLException;

    void deleteByNo(Integer id);

    void saveOrUpdate(Dept dept, boolean isUpdate) throws SQLException, ClassNotFoundException;

    String[] deptsName() throws SQLException;
}
