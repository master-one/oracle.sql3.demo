package services;

import models.Dept;
import models.Employe;
import services.api.IDeptService;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class DeptService extends BaseService implements IDeptService {

    public List<Dept> findListDepts() throws SQLException {
        List<Dept> depts = new ArrayList<>();
        ResultSet resultSet = null;
        try (Connection conn = getConnexion().openConnection();
             Statement stamt = conn.createStatement()) {
            Map<String, Class<?>> map = conn.getTypeMap();
            map.put("DEPT_T", Dept.class);
            String queryTab = "select value(od) from dept_o od";
            resultSet = stamt.executeQuery(queryTab);
            while (resultSet.next()) {
                depts.add((Dept) resultSet.getObject(1, map));
            }
        } catch (SQLException | ClassNotFoundException e) {
            e.printStackTrace();
        }
        finally {
            if(resultSet != null)
                resultSet.close();
        }
        return depts;
    }

    @Override
    public Dept getDeptByNo(int id) throws SQLException {
        ResultSet resultSet = null;
        try (Connection conn = getConnexion().openConnection();
             Statement stamt = conn.createStatement()) {
            Map<String, Class<?>> map = conn.getTypeMap();
            map.put("DEPT_T", Class.forName(Dept.class.getName()));
            map.put("EMPLOYE_T", Class.forName(Employe.class.getName()));
            String queryTab = "select value(od), od.LISTEREFEMP from dept_o od where od.DEPTNO = " + id + "";
            resultSet = stamt.executeQuery(queryTab);
            if (resultSet.next()) {
                return (Dept) resultSet.getObject(1, map);
            }

        } catch (SQLException | ClassNotFoundException e) {
            e.printStackTrace();
        }finally {
            if(resultSet != null)
                resultSet.close();
        }
        return new Dept();
    }

    @Override
    public void deleteByNo(Integer id) {

    }

    @Override
    public void saveOrUpdate(Dept dept, boolean isUpdate) throws SQLException, ClassNotFoundException {
        String requete;
        if (isUpdate)
            requete = "update dept_o set loc ='" + dept.getLoc() + "', dname = '" + dept.getDname() + "' where DEPTNO = " + dept.getDeptno();
        else
            requete = String.format("insert into v_dept2(deptno, dname, loc) values (%d, '%s', '%s')", dept.getDeptno(), dept.getDname(), dept.getLoc());

        try (Connection conn = getConnexion().openConnection();
                Statement statement=conn.createStatement()) {
            statement.executeQuery(requete);
            conn.commit();
        }
    }

    @Override
    public String[] deptsName() throws SQLException {
        List<Dept> depts = findListDepts();
        String[] deptsName = new String[depts.size()];
        int size = depts.size();
        for (int i = 0; i < size; i++) {
            deptsName[i] = depts.get(i).getDname();
        }
        return deptsName;
    }
}
