package configuration;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class Connexion {
    private String location;
    private String serviceName;
    private int    port;
    private String url;
    private String username;
    private String password;

    public Connexion() {
    }

    public Connexion(String location, String serviceName, int port, String username, String password) {
        this.setLocation(location);
        this.setServiceName(serviceName);
        this.setPort(port);
        this.setUsername(username);
        this.setPassword(password);
    }

    public void setServiceName(String serviceName) {
        this.serviceName = serviceName;
    }

    public void setPort(int port) {
        this.port = port;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public Connection openConnection() throws ClassNotFoundException, SQLException {
        String dbURL;
        if (url == null || url.equals(""))
            dbURL = url;
        else
            dbURL = String.format("jdbc:oracle:thin:@%s:%d:%s", this.location, this.port, this.serviceName);
        try {
            Class.forName("oracle.jdbc.driver.OracleDriver");
            return DriverManager.getConnection(dbURL, this.username, this.password);
        } catch (SQLException ex) {
            throw ex;
        }
    }

    public void closeConnection(Connection connex) {
        try {
            connex.close();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }
}
