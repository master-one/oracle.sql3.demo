package models;

import oracle.sql.ARRAY;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class Dept implements SQLData {
    private String        sql_type;
    private int           deptno;
    private String        dname;
    private String        loc;
    private Array         listeRefEmp;
    private List<Employe> employes = new ArrayList<>();

    /*--construct--*/
    public Dept() {
    }

    public Dept(String sql_type, int deptno, String dname, String loc, ARRAY listeRefEmp) {
        this.sql_type = sql_type;
        this.deptno = deptno;
        this.dname = dname;
        this.loc = loc;
        this.listeRefEmp = listeRefEmp;
    }

    public int getDeptno() {
        return deptno;
    }

    public void setDeptno(int deptno) {
        this.deptno = deptno;
    }

    public String getDname() {
        return dname;
    }

    public void setDname(String dname) {
        this.dname = dname;
    }

    public String getLoc() {
        return loc;
    }

    public void setLoc(String loc) {
        this.loc = loc;
    }

    private Array getListeRefEmp() {
        return listeRefEmp;
    }

    public List<Employe> getEmployes() {
        return employes;
    }

    private void setListeRefEmp(Array listeRefEmp) throws SQLException {
        Ref[] array = (Ref[]) listeRefEmp.getArray();
        for (Ref o : array) {
            Object res = o.getObject();
            if(res.getClass().getSimpleName().equals("STRUCT"))
                break;
            employes.add((Employe) res);
        }
        this.listeRefEmp = listeRefEmp;
    }

    @Override
    public String getSQLTypeName() {
        return this.sql_type;
    }

    @Override
    public void readSQL(SQLInput stream, String typeName) throws SQLException {
        this.sql_type = typeName;
        this.setDeptno(stream.readInt());
        this.setDname(stream.readString());
        this.setLoc(stream.readString());
        this.setListeRefEmp(stream.readArray());
    }

    @Override
    public void writeSQL(SQLOutput stream) throws SQLException {
        stream.writeInt(this.getDeptno());
        stream.writeString(this.getDname());
        stream.writeString(this.getLoc());
        stream.writeArray(this.getListeRefEmp());
    }
}
