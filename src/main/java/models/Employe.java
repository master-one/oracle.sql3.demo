package models;

import oracle.sql.ARRAY;

import java.io.BufferedReader;
import java.io.IOException;
import java.sql.*;

public class Employe implements SQLData {
    private String sql_type;
    private int    empno;
    private String ename;
    private ARRAY  prenoms;
    private String job;
    private double sal;
    private Clob   cv;
    private Date   date_naiss;
    private Date   date_emb;
    private Ref    refDept;
    private Dept   dept;
    public  String lesprenoms;
    public  String textcv;

    /*--construct--*/
    public Employe() {
    }

    public Employe(String sql_type, int empno, String ename, ARRAY prenoms, String job, double sal, Clob cv, Date date_naiss, Date date_emb, Ref refDept) {
        this.sql_type = sql_type;
        this.empno = empno;
        this.ename = ename;
        this.prenoms = prenoms;
        this.job = job;
        this.sal = sal;
        this.cv = cv;
        this.date_naiss = date_naiss;
        this.date_emb = date_emb;
        this.refDept = refDept;
    }

    /*--construct--*/
    /*--get & set--*/
    public String getTextcv() {
        return this.textcv;
    }

    public void setTextcv(String cv) {
        this.textcv = cv;
    }

    public String getLesprenoms() {
        return lesprenoms;
    }

    public void setLesprenoms(String lesprenomss) {
        this.lesprenoms = lesprenomss;
    }

    public int getEmpno() {
        return empno;
    }

    public void setEmpno(int empno) {
        this.empno = empno;
    }

    public String getEname() {
        return ename;
    }

    public void setEname(String ename) {
        this.ename = ename;
    }

    public ARRAY getPrenoms() {
        return prenoms;
    }

    public void setPrenoms(ARRAY prenoms) throws SQLException {
        String[] o = (String[]) prenoms.getArray();
        this.lesprenoms = "";
        for (String s : o) {
            this.lesprenoms += " " + s;
        }
        this.prenoms = prenoms;
    }

    public String getJob() {
        return job;
    }

    public void setJob(String job) {
        this.job = job;
    }

    public double getSal() {
        return sal;
    }

    public void setSal(double sal) {
        this.sal = sal;
    }

    public Clob getCv() {
        return cv;
    }

    private void setCv(Clob cv) throws SQLException {
        try {
            BufferedReader clobReader = new BufferedReader(cv.getCharacterStream());
            String         ligne;
            this.textcv = "";
            while ((ligne = clobReader.readLine()) != null) {
                this.textcv += " " + ligne;
            }
            this.cv = cv;
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public Date getDate_naiss() {
        return date_naiss;
    }

    public void setDate_naiss(Date date_naiss) {
        this.date_naiss = date_naiss;
    }

    public Date getDate_emb() {
        return date_emb;
    }

    public void setDate_emb(Date date_emb) {
        this.date_emb = date_emb;
    }

    public Ref getRefDept() {
        return refDept;
    }

    private void setRefDept(Ref refDept) throws SQLException {
        this.refDept = refDept;
    }

    public Dept getDept() {
        return dept;
    }

    public void setDept(Dept dept) {
        this.dept = dept;
    }

    /*--get & set--*/

    @Override
    public String getSQLTypeName() throws SQLException {
        return this.sql_type;
    }

    @Override
    public void readSQL(SQLInput stream, String typeName) throws SQLException {
        this.sql_type = typeName;
        this.setEmpno(stream.readInt());
        this.setEname(stream.readString());
        this.setPrenoms((ARRAY) stream.readArray());
        this.setJob(stream.readString());
        this.setSal(stream.readDouble());
        this.setCv(stream.readClob());
        this.setDate_naiss(stream.readDate());
        this.setDate_emb(stream.readDate());
        this.setRefDept(stream.readRef());
    }

    @Override
    public void writeSQL(SQLOutput stream) throws SQLException {
        stream.writeInt(this.getEmpno());
        stream.writeString(this.getEname());
        stream.writeArray(this.getPrenoms());
        stream.writeString(this.getJob());
        stream.writeDouble(this.getSal());
        stream.writeClob(this.getCv());
        stream.writeDate(this.getDate_naiss());
        stream.writeDate(this.getDate_emb());
        stream.writeRef(this.getRefDept());
    }
}
