<nav class="navbar navbar-expand navbar-light bg-white topbar mb-4 static-top ">
    <h1 style="margin:0 auto;">Dept_o & Employe_o</h1>
</nav>
<div class="container-fluid">
    <div class="row justify-content-center">
        <div class="col-xl-10 col-lg-12 col-md-9">
            <div class="card-body p-0">
                <div class="row">
                    <div class="col-lg-2"></div>
                    <div class="col-lg-8 card">
                        <div class="p-5">
                            <div class="text-center">
                                <h1 class="h4 text-gray-900 mb-4">Nouveau employ&eacute;</h1>
                            </div>
                            <form method="POST" class="user" enctype="multipart/form-data">
                                <div class="form-group">
                                    <label> EMPNO <span style="color: red">*</span>:</label>
                                    <input type="text" name="empno" class="form-control" required>
                                </div>
                                <div class="form-group">
                                    <label> ENAME :</label>
                                    <input type="text" name="ename" class="form-control" required>
                                </div>
                                <div class="form-group">
                                    <label> PRENOMS :</label>
                                    <input type="text" name="prenoms" class="form-control" required>
                                </div>
                                <div class="form-group">
                                    <label> JOB :</label>
                                    <select name="job" class="form-control selectpicker" data-live-search="true">
                                        <option value="">Veuillez choisir parmis les m&eacute;tiers suivant</option>
                                        <option value="Ing&eacute;nieur">Ing&eacute;nieur</option>
                                        <option value="Secr&eacute;taire">Secr&eacute;taire</option>
                                        <option value="Directeur">Directeur</option>
                                        <option value="Planton">Planton</option>
                                        <option value="PDG">PDG</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label> SALAIRE :</label>
                                    <input type="number" name="salaire" class="form-control">
                                </div>
                                <div class="form-group">
                                    <label> CV :</label>
                                    <input type="file" name="cv" >
                                </div>
                                <div class="form-group">
                                    <label> DATE DE NAISSANCE :</label>
                                    <input type="date" name="date_naiss" class="form-control" >
                                </div>
                                <div class="form-group">
                                    <label> DATE D' EMBAUCHE :</label>
                                    <input type="date" name="date_emb" class="form-control">
                                </div>
                                <div class="form-group">
                                    <label> DEPARTEMENT :</label>
                                    <select name="departement" class="form-control selectpicker" data-live-search="true">
                                        <option value="">Veuillez choisir parmis les departement suivant</option>
                                        <option value="1">1000 - RH</option>
                                        <option value="1">1001 - Marketing</option>
                                    </select>
                                </div>
                                <input type="submit" value="Ajouter" class="btn btn-primary btn-user btn-block">
                            </form>
                        </div>
                    </div>
                    <div class="col-lg-2"></div>
                </div>
            </div>
        </div>
    </div>
</div>
