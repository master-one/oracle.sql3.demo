<nav class="navbar navbar-expand navbar-light bg-white topbar mb-4 static-top ">
    <h1 style="margin:0 auto;">Dept_o & Employe_o</h1>
</nav>
<div class="container">
    <div class="row">
        <div class="col-xl-6 col-lg-6 col-md-6 mb-4">
            <div class="card border-left-info  h-100 py-2">
                <div class="card-body">
                    <div class="row no-gutters align-items-center">
                        <div class="col mr-2">
                            <div class="text-xs font-weight-bold text-info text-uppercase mb-1">Nombre de departements
                            </div>
                            <div class="row no-gutters align-items-center">
                                <div class="col-auto">
                                    <div class="h5 mb-0 mr-3 font-weight-bold text-gray-800">5</div>
                                </div>

                            </div>
                        </div>
                        <div class="col-auto">
                            <i class="fas fa-clipboard-list fa-2x text-gray-300"></i>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xl-6 col-lg-6 col-md-6 mb-4">
            <div class="card border-left-primary  h-100 py-2">
                <div class="card-body">
                    <div class="row no-gutters align-items-center">
                        <div class="col mr-2">
                            <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">Nombre d' employes
                            </div>
                            <div class="h5 mb-0 font-weight-bold text-gray-800">40</div>
                        </div>
                        <div class="col-auto">
                            <i class="fas fa-calendar fa-2x text-gray-300"></i>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="container-fluid">
    <div class="row justify-content-center">
        <div class="col-12">
            <div class="text-center">
                <h4 class="text-success">Message Flash</h4>
            </div>
        </div>
        <div class="col-xl-10 col-lg-12 col-md-9">
            <div class="card-body p-0">
                <div class="row">
                    <div class="col-lg-2"></div>
                    <div class="col-lg-8 card">
                        <div class="p-5">
                            <div class="text-center">
                                <h1 class="h4 text-gray-900 mb-4">Configuration</h1>
                            </div>
                            <form method="POST" class="user">
                                <div class="form-group">
                                    <label> Nom utilisateur <span style="color: red">*</span>:</label>
                                    <input type="text" name="utilisateur" class="form-control" required>
                                </div>
                                <div class="form-group">
                                    <label> Mot de passe <span style="color: red">*</span> :</label>
                                    <input type="password" name="mdp" class="form-control" required>
                                </div>
                                <div class="form-group">
                                    <label> Port <span style="color: red">*</span> :</label>
                                    <input type="number" name="port" class="form-control" value="443" required>
                                </div>
                                <div class="form-group">
                                    <label> H&ocirc;te <span style="color: red">*</span> :</label>
                                    <input type="number" name="hote" class="form-control" value="134.59.152.116"
                                           required>
                                </div>
                                <div class="form-group">
                                    <label> Nom de service <span style="color: red">*</span> :</label>
                                    <input type="number" name="service" class="form-control" value="134.59.152.116"
                                           required>
                                </div>
                                <input type="submit" value="Changer" class="btn btn-primary btn-user btn-block">
                            </form>
                        </div>
                    </div>
                    <div class="col-lg-2"></div>
                </div>
            </div>
        </div>
    </div>
</div>