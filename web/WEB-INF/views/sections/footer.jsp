<footer class="sticky-footer bg-white">
    <div class="container my-auto">
        <div class="copyright text-center my-auto">
            <span>Copyright &copy; by Olivier, Valery, Alexandros</span>
        </div>
    </div>
</footer>