<ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">
    <a class="sidebar-brand d-flex align-items-center justify-content-center" href="${pageContext.request.contextPath}/">
        <div class="sidebar-brand-icon rotate-n-15">
            <i class="fas fa-laugh-wink"></i>
        </div>
        <div class="sidebar-brand-text mx-3">Projet</div>
    </a>
    <hr class="sidebar-divider my-0">
    <li class="nav-item">
        <a class="nav-link" href="${pageContext.request.contextPath}/list-dept">
            <i class="fas fa-fw fa-home"></i>
            <span>Departements</span>
        </a>
    </li>
    <li class="nav-item">
        <a class="nav-link" href="${pageContext.request.contextPath}/list-employe">
            <i class="fas fa-fw fa-users"></i>
            <span>Employ&eacute;s</span>
        </a>
    </li>
    <hr class="sidebar-divider">
    <li class="nav-item">
        <a class="nav-link" href="${pageContext.request.contextPath}/configuration-data-base">
            <i class="fas fa-fw fa-cog"></i>
            <span>Configuration</span>
        </a>
    </li>
</ul>