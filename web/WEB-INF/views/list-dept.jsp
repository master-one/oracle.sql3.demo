<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<nav class="navbar navbar-expand navbar-light bg-white topbar mb-4 static-top ">
    <h1 style="margin:0 auto;">Dept_o & Employe_o</h1>
</nav>
<div class="container-fluid">
    <div class="row entete">
        <div class="col-12">
            <div class="text-center">
                <h4 class="text-success">Message Flash</h4>
            </div>
        </div>
        <div class="col-6">
            <h1 class="h3 mb-2 text-gray-800">Les d&eacute;partements</h1>
        </div>
        <div class="col-6">
            <a style="float: right;" class="btn btn-success" href="${pageContext.request.contextPath}/create-dept"><i class="fas fa-fw fa-plus-circle"></i>Ajouter</a>
        </div>
    </div>

    <div class="card  mb-4">
        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-bordered">
                    <thead>
                    <tr>
                        <th>DEPTNO</th>
                        <th>DNAME</th>
                        <th>LOCATION</th>
                        <th>Employes</th>
                        <th>Modifier</th>
                        <th>Supprimer</th>
                    </tr>
                    </thead>
                    <tbody>
                    <c:forEach var="name" items="${depts}">
                        <tr>
                            <td>${name.deptno}</td>
                            <td>${name.dname}</td>
                            <td>${name.loc}</td>
                            <td>
                                <a href="${pageContext.request.contextPath}/info-dept?id=${name.deptno}">
                                    <i class="fa fa-info"></i>
                                </a>
                            </td>
                            <td>
                                <a href="${pageContext.request.contextPath}/update-dept/${name.deptno}">
                                    <i class="fa fa-edit"></i>
                                </a>
                            </td>
                            <td>
                                <a class="text-danger" href="${pageContext.request.contextPath}/delete-dept/${name.deptno}">
                                    <i class="fa fa-trash"></i>
                                </a>
                            </td>
                        </tr>
                    </c:forEach>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
