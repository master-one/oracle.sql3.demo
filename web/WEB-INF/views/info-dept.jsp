<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<nav class="navbar navbar-expand navbar-light bg-white topbar mb-4 static-top ">
    <h1 style="margin:0 auto;">Dept_o & Employe_o</h1>
</nav>
<div class="container-fluid">
    <div class="row entete">
        <div class="col-4"></div>
        <div class="col-4">
            <h1 class="h4 mb-2 text-gray-800">Info d&eacute;partement</h1>
            <table class="table table-bordered">
                <thead>
                <tr>
                    <th>DEPTNO</th>
                    <td>${dept.deptno}</td>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <th>DNAME</th>
                    <td>${dept.dname}</td>
                </tr>
                <tr>
                    <th>LOCATION</th>
                    <td>${dept.loc}</td>
                </tr>
                </tbody>
            </table>
        </div>
        <div class="col-4"></div>
    </div>

    <div class="card mb-4">
        <div class="card-body">
            <h1 class="h4 mb-2 text-gray-800">Liste des employ&eacute;s</h1>
            <div class="table-responsive">
                <table class="table table-bordered" <%--id="dataTable"--%>>
                    <thead>
                    <tr>
                        <th>EMP_NO</th>
                        <th>E_NAME</th>
                        <th>PRENOMS</th>
                        <th>JOB</th>
                        <th>SAL</th>
                        <th>CV</th>
                        <th>DATE_NAISS</th>
                        <th>DATE_EMB</th>
                    </tr>
                    </thead>
                    <tbody>
                    <c:forEach var="employe" items="${employes}">
                        <tr>
                            <td>${employe.empno}</td>
                            <td>${employe.ename}</td>
                            <td>${employe.lesprenoms}</td>
                            <td>${employe.job}</td>
                            <td>${employe.sal} $</td>
                            <td>${employe.textcv}</td>
                            <td>${employe.date_naiss}</td>
                            <td>${employe.date_emb}</td>
                        </tr>
                    </c:forEach>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
