<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<nav class="navbar navbar-expand navbar-light bg-white topbar mb-4 static-top ">
    <h1 style="margin:0 auto;">Dept_o & Employe_o</h1>
</nav>
<div class="container-fluid">
    <div class="row entete">
        <div class="col-12">
            <div class="text-center">
                <h4 class="text-success">Message Flash</h4>
            </div>
        </div>
        <div class="col-6">
            <h1 class="h3 mb-2 text-gray-800">Les employ&eacute;s</h1>
        </div>
        <div class="col-6">
            <a style="float: right;" class="btn btn-success" href="${pageContext.request.contextPath}/create-employe"><i class="fas fa-fw fa-plus-circle"></i>Ajouter</a>
        </div>
    </div>

    <div class="card  mb-4">
        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-responsive-lg table-bordered" <%--id="dataTable"--%>>

                    <thead>
                    <tr>
                        <th>EMPNO</th>
                        <th>ENAME</th>
                        <th>PRENOMS</th>
                        <th>JOB</th>
                        <th>SAL</th>
                        <th>CV</th>
                        <th>DATE_NAISS</th>
                        <th>DATE_EMB</th>
                        <th>Departement</th>
                        <th>Edit.</th>
                        <th>Supp.</th>
                    </tr>
                    </thead>
                    <tbody>
                    <c:forEach items="${employes}" var="employe">
                        <tr>
                            <td>${employe.empno}</td>
                            <td>${employe.ename}</td>
                            <td>${employe.lesprenoms}</td>
                            <td>${employe.job}</td>
                            <td>${employe.sal} $</td>
                            <td>${employe.textcv}</td>
                            <td>${employe.date_naiss}</td>
                            <td>${employe.date_emb}</td>
                            <td>${employe.date_emb}</td>
                            <td>
                                <a>
                                    <i class="fa fa-edit"></i>
                                </a>
                            </td>
                            <td>
                                <a data-toggle="modal" data-target="#deleteModal">
                                    <i class="fa fa-trash"></i>
                                </a>
                            </td>
                        </tr>
                    </c:forEach>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
