<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<nav class="navbar navbar-expand navbar-light bg-white topbar mb-4 static-top ">
    <h1 style="margin:0 auto;">Dept_o & Employe_o</h1>
</nav>
<div class="container-fluid">
    <div class="row justify-content-center">
        <div class="col-xl-10 col-lg-12 col-md-9">
            <div class="card-body p-0">
                <div class="row">
                    <div class="col-lg-2"></div>
                    <div class="col-lg-8 card">
                        <div class="p-5">
                            <div class="text-center">
                                <h1 class="h4 text-gray-900 mb-4">Nouveau d&eacute;partement</h1>
                            </div>
                            <form method="POST" action="${pageContext.request.contextPath}/insert-or-update-dept/${dept.deptno != 0}" class="user">
                                <div class="form-group">
                                    <label> DEPTNO <span style="color: red">*</span>:</label>
                                    <input type="text" value="${dept.deptno}" name="deptno" class="form-control" required>
                                </div>
                                <div class="form-group">
                                    <label> DNAME <span style="color: red">*</span> :</label>
                                    <c:choose>
                                        <c:when test="${dept.deptno == 0}">
                                            <input type="text" value="${dept.dname}" name="dname" class="form-control" required>
                                        </c:when>
                                        <c:otherwise>
                                            <select name="dname" class="form-control selectpicker" data-live-search="true">
                                                <option value="">Veuillez choisir parmis les noms suivant</option>
                                                <c:forEach var="name" items="${deptsName}">
                                                    <option <c:if test="${name == dept.dname}">selected</c:if> value="${name}">${name}</option>
                                                </c:forEach>
                                            </select>
                                        </c:otherwise>
                                    </c:choose>
                                </div>
                                <div class="form-group">
                                    <label> LOC <span style="color: red">*</span> :</label>
                                    <input type="text" value="${dept.loc}" name="loc" class="form-control" required>
                                </div>
                                <c:choose>
                                    <c:when test="${dept.deptno == 0}">
                                        <input type="submit" value="Ajouter" class="btn btn-primary btn-user btn-block">
                                    </c:when>
                                    <c:otherwise>
                                        <input type="submit" value="Modifier" class="btn btn-primary btn-user btn-block">
                                    </c:otherwise>
                                </c:choose>
                            </form>
                        </div>
                    </div>
                    <div class="col-lg-2"></div>
                </div>
            </div>
        </div>
    </div>
</div>
